package main

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"
	"os"
)

var context = struct {
	db   *sql.DB
	port string
}{
	db:   nil,
	port: os.Getenv("SERVER_PORT"),
}

func main() {
	var err error

	context.db = createDb()
	defer func() {
		err = errors.Join(err, context.db.Close())
	}()
	migrateDb(context.db)

	fmt.Printf("Server running on port %s\n", context.port)
	err = createServer().ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func createServer() *http.Server {
	mux := http.NewServeMux()

	mux.HandleFunc("/ping", Ping)
	mux.HandleFunc("GET /api/users", GetUsers)

	server := &http.Server{
		Addr:    ":" + context.port,
		Handler: enableCors(mux),
	}

	return server
}
