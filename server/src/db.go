package main

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	"os"

	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
)

var dbConfig = struct {
	host, port, user, password, dbname, migrationPath string
}{
	host:          os.Getenv("PG_HOST"),
	port:          os.Getenv("PG_PORT"),
	user:          os.Getenv("PG_USER"),
	password:      os.Getenv("PG_PASSWORD"),
	dbname:        os.Getenv("PG_DBNAME"),
	migrationPath: os.Getenv("PG_MIGRATION_PATH"),
}

func createDb() *sql.DB {
	connString := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		dbConfig.host,
		dbConfig.port,
		dbConfig.user,
		dbConfig.password,
		dbConfig.dbname,
	)

	fmt.Printf("Trying to connect: %s\n", connString)

	db, err := sql.Open("postgres", connString)
	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	fmt.Println("Successfully connected")

	return db
}

func migrateDb(db *sql.DB) {
	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		panic(err)
	}

	sourceUrl := fmt.Sprintf("file://%s", dbConfig.migrationPath)
	m, err := migrate.NewWithDatabaseInstance(sourceUrl, "postgres", driver)
	if err != nil {
		panic(err)
	}

	err = m.Up()
	if err != nil && !errors.Is(err, migrate.ErrNoChange) {
		panic(err)
	}
}
