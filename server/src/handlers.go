package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

type User struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

func GetUsers(writer http.ResponseWriter, _ *http.Request) {
	rows, err := context.db.Query("SELECT * FROM users")
	if err != nil {
		fmt.Printf("Query execution failed: %s\n", err.Error())
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer func() {
		err = errors.Join(err, rows.Close())
	}()

	var users []User
	for rows.Next() {
		u := User{}
		err := rows.Scan(&u.Id, &u.Name)
		if err != nil {
			fmt.Printf("Row parsing failed: %s\n", err.Error())
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}
		users = append(users, u)
	}

	jsonResp, err := json.MarshalIndent(users, "", "\t")
	if err != nil {
		fmt.Printf("Marshal failed: %s\n", err.Error())
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}

	writer.Header().Set("Content-Type", "application/json")
	_, err = writer.Write(jsonResp)
	if err != nil {
		fmt.Printf("Write failed: %s\n", err.Error())
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func Ping(writer http.ResponseWriter, _ *http.Request) {
	writer.WriteHeader(http.StatusOK)
}
