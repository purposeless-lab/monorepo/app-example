const getJson = async (url) => {
    console.log(url)
    const response = await fetch(url);
    console.log(response)
    if (!response.ok) {
        throw new Error(response.statusText);
    }
    return response.json();
}

const p = document.getElementById("response")
p.innerHTML = "Waiting for a response text";

const host = window.location.protocol + "//" +
             window.location.host;
console.log(`Fetching data: ${host}`);
getJson(`${host}/api/users`)
    .then(data => {
        console.log(JSON.stringify(data));
        p.innerHTML = JSON.stringify(data);
    })
    .catch(error => {
        console.error(error);
        p.innerHTML = error;
    });
