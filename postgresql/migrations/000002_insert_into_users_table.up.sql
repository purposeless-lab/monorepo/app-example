INSERT INTO users
    (id, name)
VALUES
    (1, 'Michael'),
    (2, 'Daniel'),
    (3, 'Dmitriy'),
    (4, 'Andrey')
ON CONFLICT DO NOTHING;